

python3 pool.py -h


usage: 
    ----------------------------
    #
    python3 pool.py make_offline

    #
    python3 pool.py sign_offline

    #
    python3 pool.py broadcast_online
    ----------------------------

    ----------------------------
    #
    python3 pool.py make_online

    #
    python3 pool.py broadcast_online
    ----------------------------
    

LISK Pool v1

positional arguments:
  {make_offline,sign_offline,save_offline,broadcast_offline,make_online,broadcast_online}

optional arguments:
  -h, --help            show this help message and exit
  --second-pass         Second passphrase flag
  -d, --dry-run         Dry run flag
  --offline
  --online
  -t TIMESTAMP, --timestamp TIMESTAMP
                        Timestamp to start
  -f FILENAME, --filename FILENAME
                        Config file. Default is ./config.yaml
  -l {info,debug,warning,error,critical}, --log {info,debug,warning,error,critical}
                        Logging facility


#!/usr/bin/env python3

import time
import logging
import requests
import sqlite3
from .constants import constants

log = logging.getLogger(__name__)

class utils():
    """

    """

    def api_get(url):
        """
        Helper method for api get requests

        Args:
            url      (str ): url to request
        Returns:
            response (dict): dictionary of the api response
            response (None): return on exception
        """
        log.debug(url)

        try:
            response = requests.get(url).json()
        except:
            log.error("api get failed on {}".format(url))
            response = None

        return response

    def api_post():
        """

        """

        pass

    def api_put():
        """

        """

        pass

    def log_to_file(logname, logger_name, save_dir):
        """

        """
        #date_time = time.strftime("%m-%d-%Y")
        logger = logging.getLogger(logger_name)
        hdlr = logging.FileHandler('./{}/{}.log'.format(save_dir, logname))
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.INFO)

        return logger

    def open_db():
        """
        Open connection to the sqlite database
        """
        conn = sqlite3.connect(constants.db_file)

        cursor = conn.cursor()

        return cursor, conn

    def close_db(conn):
        """

        """

        conn.close()

    def log_to_db():
        """
        Log information to db
        """
        pass

    def db_execute(conn, cursor, statement):
        """
        Write a line to the database
        """
        try:
            cursor.execute(statement)
        except:
            raise

        return cursor.fetchall()

    def write_to_db():
        """
        Write a line to the database
        """
        pass

    def read_from_db(conn, cursor, statement):
        """
        Read information from the database
        """
        cursor.execute(statement)

        column_names = [desc[0] for desc in cursor.description]

        results = []

        for row in cursor:
            new_row = [x for x in row]
            results.append(dict(zip(column_names, new_row)))

        return results

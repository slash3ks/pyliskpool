#!/usr/bin/env python3

import sys
import json
import time
import math
import argparse
import getpass
import struct
import binascii
import hashlib
import pysodium
from collections import OrderedDict
from io import BytesIO
try:
    import requests
except ImportError:
    pass

def get_pub_priv_key(secret1, secret2):
    """
    """
    seed = hashlib.sha256(secret1.encode('utf8')).digest()
    pubk, privk = pysodium.crypto_sign_seed_keypair(seed)
    del seed

    pubk2 = None
    privk2 = None

    #Check if second signature
    if secret2:
        seed2 = hashlib.sha256(secret2.encode('utf8')).digest()
        pubk2, privk2 = pysodium.crypto_sign_seed_keypair(seed2)
        del seed2

    return pubk, privk, pubk2, privk2

def gen_tx(pubk, privk, pubk2, privk2, amount, recipient, genesis_t, fee):
    """
    """
    transaction = OrderedDict()
    buf = BytesIO()

    # Write transaction type and timestamp to buffer
    transaction["type"] = 0
    transaction["timestamp"] = int(time.time()) - genesis_t
    buf.write(struct.pack("<bi", transaction['type'], int(transaction['timestamp'])))

    transaction["asset"] = {}

    # Write sender pubkey
    buf.write(struct.pack("!{}s".format(len(pubk)), pubk))

    # Add sender public key from the transaction
    transaction['senderPublicKey'] = binascii.hexlify(pubk).decode('utf-8')

    # Need to cut out the L from the ID
    transaction["recipientId"] = recipient
    recipient_id = int(transaction["recipientId"][:-1])
    buf.write(struct.pack(">Q", int(recipient_id)))

    # Write the tx amount
    transaction["amount"] = "{}".format(int(amount * math.pow(10, 8)))
    transaction["fee"] = "{}".format(fee)
    buf.write(struct.pack("<Q", int(transaction["amount"])))

    # Sign the transaction with the private key
    signature = pysodium.crypto_sign_detached(hashlib.sha256(buf.getvalue()).digest(), privk)
    del privk

    # Get the signature in string hash form
    transaction['signature'] = binascii.hexlify(signature).decode('utf-8')

    # Write signature to buffer
    buf.write(struct.pack("!{}s".format(len(signature)), signature))

    #Check if second signature
    if pubk2:

        # Sign the transaction with the private key
        second_sig = pysodium.crypto_sign_detached(hashlib.sha256(buf.getvalue()).digest(), privk2)
        transaction['signSignature'] = binascii.hexlify(second_sig).decode('utf-8')

        # Write signature to buffer
        buf.write(struct.pack("!{}s".format(len(second_sig)), second_sig))
        del privk2

    # Get all the data and close the buffer
    result = buf.getvalue()
    buf.close()

    # Get ID
    tx_id = hashlib.sha256(result).digest()

    # Add the ID to the transaction
    transaction['id'] = "%s" % struct.unpack("<Q", tx_id[:8])

    return transaction

def send_tx(url, data, headers):
    """
    """
    try:
        response = requests.post(url + "/api/transactions",
                                 headers=headers, data=json.dumps(data))
        print(json.dumps(response.json(), indent=3))
    except:
        raise


def get_constants(url):
    """
    """
    api_endpoint = '/api/node/constants'
    try:
        response = requests.get(url + api_endpoint)
        return response.json()
    except:
        raise

def get_file_data(file_path):
    """
    """
    try:
        with open(file_path) as jfh:
            data = json.load(jfh)
            return data
    except IOError:
        sys.exit('Could not open specified file')

def send_online(args):
    """
    """
    # Define the header and the static arguments
    headers = {
        "Content-Type": "application/json"
    }

    # Is there a way to get the url?
    if not args.url:
        sys.exit("Please provide a url --url")

    # Get constants
    const = get_constants(args.url)

    # Get the version and nethash from the API
    headers['version'] = const['data']['version']
    headers['nethash'] = const['data']['nethash']

    # Get the transaction file
    if not args.file_path:
        sys.exit("Please provide a tx file path --file-path")
    else:
        data = get_file_data(args.file_path)

    resp = send_tx(args.url, data['transactions'][0], headers)

def gen_offline(args):
    """
    """
    if not args.recipient or not args.amount:
        sys.exit("--recipient-id and/or --amount are required")

    secret1a = None
    secret2a = None

    try:
        print("Please the first passphrase:")
        secret1a = getpass.getpass()
        print("Confirming the passphrase. Please type/paste it again.")
        secret1b = getpass.getpass()

        # Simple check. needs revamp
        if secret1a != secret1b:
            sys.exit("Passprase does not match. Please try again")
        else:
            del secret1b

        if args.second_secret:

            print("Please the second passphrase:")
            secret2a = getpass.getpass()
            print("Confirming the second passphrase. Please type/paste it again.")
            secret2b = getpass.getpass()

            # Simple check. needs revamp
            if secret2a != secret2b:
                sys.exit("Passprase does not match. Please try again")
            else:
                del secret2b
    except KeyboardInterrupt:
        sys.exit("Good Bye Steven")

    pubk, privk, pubk2, privk2 = get_pub_priv_key(secret1a,
                                                  secret2a)
    resp = gen_tx(pubk, privk, pubk2, privk2,
                  args.amount, args.recipient,
                  args.genesis_timestamp, args.fee)

    print(json.dumps(resp))

def cut_transactions(args):
    """
    """
    data = get_file_data(args.file_path)

    transactions = data['transactions']

    # Save the transaction object to file
    step = 24
    tx_new = []
    if len(transactions) <= 25:
        with open('transaction{}.json'.format(step), 'w') as jfh:
            json.dump({"transactions":transactions}, jfh)
            sys.exit()

    for idx, transaction in enumerate(transactions):
        tx_new.append(transaction)
        if idx == step:
            with open('transaction{}.json'.format(step), 'w') as jfh:
                json.dump({"transactions":tx_new}, jfh)
            step += 25
            tx_new = []

    with open('transactionlast.json'.format(step), 'w') as jfh:
        json.dump({"transactions":tx_new}, jfh)

def main(args):
    """
    Checks, balances and passphrase logic in here
    """
    if args.option == 'send':
        send_online(args)
    elif args.option == 'offline':
        gen_offline(args)
    elif args.option == 'cut':
        cut_transactions(args)

def usage():
    """
    """
    return '''
    #
    python3 pyliskcrypt.py send --url https://node01.lisk.io --file-path ./tx.json

    #
    python3 pyliskcrypt.py cut --file-path ./tx.json

    #
    python3 pyliskcrypt.py offline --recipient-id 1234L --amount 5 --timestamp 1464109200 --fee 10000000

    #
    python3 pyliskcrypt.py offline --recipient-id 1234L --amount 5 --timestamp 1464109200 --fee 10000000 --second-secret
    '''

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(usage=usage())

    # TX gen vars
    PARSER.add_argument(dest='option',
                        action='store',
                        choices=('send', 'offline', 'cut'),
                        help='')

    PARSER.add_argument('-r','--recipient-id',
                        dest='recipient',
                        action='store',
                        help='')

    PARSER.add_argument('-a',
                        '--amount',
                        dest='amount',
                        action='store',
                        type=int,
                        default=0,
                        help='')

    PARSER.add_argument('-t',
                        '--timestamp',
                        dest='genesis_timestamp',
                        action='store',
                        type=int,
                        default=1464109200,
                        help='')

    PARSER.add_argument('--fee',
                        dest='fee',
                        action='store',
                        type=int,
                        default=10000000,
                        help='')

    PARSER.add_argument('-s',
                        '--second-secret',
                        dest='second_secret',
                        action='store_true',
                        help='')

    # Send vars
    PARSER.add_argument('-f','--file-path',
                        dest='file_path', action='store',
                        help='File containing the transaction(s)')

    PARSER.add_argument('-u','--url', dest='url', action='store',
                        help='Where do we send this transaction?')

    PARSER.add_argument('-n','--nethash', dest='nethash', action='store',
                        help='')

    PARSER.add_argument('--version', dest='version', action='store',
                        help='')

    ARGS = PARSER.parse_args()

    # Sub Main Bro
    main(ARGS)

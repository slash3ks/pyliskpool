#!/usr/bin/env python3

import sys
import yaml

class constants():
    """
    Constant variables
    """
    try:
        with open('./config.yaml') as fh:
            data = yaml.safe_load(fh)
        with open('./exception_list') as el:
            ignore = yaml.safe_load(el)
    except IOError:
        raise
        sys.exit("Cannot open configuration file in constants.py")

    url = data.get('url')

    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
    }

    getforgedby_account = '/api/delegates/{}/forging_statistics?fromTimestamp={}'
    getaccount_voters = '/api/voters?username='
    transactions_uri = '/api/transactions'

    min_payout = data.get('min_payout')

    db_file = './pyliskpool.sqlite'

    genesis_timestamp = 1464109200
    fee = 10000000
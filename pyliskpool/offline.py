import os
import sys
import json
import pyliskpool.pyliskcrypt as pyliskcrypt
from .constants import constants

class offline():
    """
    """
    pass

    def generate_transactions(secret1, secret2, data):
        """
        """
        pubk, privk, pubk2, privk2 = pyliskcrypt.get_pub_priv_key(secret1, secret2)

        timestamp = constants.genesis_timestamp
        fee = constants.fee

        transactions = [pyliskcrypt.gen_tx(pubk,privk,pubk2,privk2,x['payout_amount'],
                        x['address'],timestamp,fee) for x in data]

        return transactions

    def dump_to_file(data, save_dir):
        """
        """
        transactions = [json.loads(x[0]) for x in data]

        # Save the transaction object to file
        with open('{}/transactions-master.json'.format(save_dir), 'w') as jfh:
            json.dump({"transactions": transactions}, jfh)

        # Save the transaction object to file
        step = 25
        for i in range(0, len(transactions), step):
            with open('{}/tx{}.json'.format(save_dir, i), 'w') as jfh:
                json.dump({"transactions": transactions[i:i+step]}, jfh)

        return True

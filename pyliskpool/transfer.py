#!/usr/bin/env python3

import json
import math
import time
import requests
import logging
from .constants import constants
from .utils import utils

LOG = logging.getLogger(__name__)

class transfer(object):
    """
    Transfer methods class
    """
    def online(data, interval, conn, cursor, tablename):
        """
        Grab transactions from timed table then broadcast
        """
        address = constants.url + constants.transactions_uri

        for payload, row_id in data:

            response = requests.post(address, headers=constants.headers, data=payload)

            LOG.info(response.json())

            if response.status_code == 200:

                cursor.execute('''
                    UPDATE "{}" SET sent = 1
                    WHERE id = ? '''.format(tablename),
                    (row_id,))

                conn.commit()

            time.sleep(interval)

        return

    def offline(data, save_dir):
        """
        Grab transactions from main file then broadcast
        """
        pass

    def to_db(pay_wave, cursor, conn, dry_run):
        """
        """
        create_if_not_exists = """CREATE TABLE IF NOT EXISTS transactions(
                address TEXT,
                payout_amount int,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"""
        cursor.execute(create_if_not_exists)
        conn.commit()

        if dry_run is False:
            for item in pay_wave:
                cursor.execute('''
                    INSERT INTO transactions(address, payout_amount)
                    VALUES(?,?)''',
                    (item['address'], item['payout_amount']))

            conn.commit()

        return True

    def from_db(cursor, conn):
        """
        """
        statement = 'SELECT * FROM transactions'

        cursor.execute(statement)

        column_names = [desc[0] for desc in cursor.description]

        results = []

        for row in cursor:
            new_row = [x for x in row]
            results.append(dict(zip(column_names, new_row)))

        return results

    def payouts(cursor, conn, vote_list, delay, secret, second_secret=None, dry_run=None):
        """
        Transfer rewards to the delegates

        Args:
            vote_list     (list ): list of voters (dct) amount and address
            delay         (float): transmission delay
            secret        (str  ): sending account secret
            second_secret (str  ): second secret of account if applicable
        Returns:
            responses     (list ): list of all resps from the txs to the API
            payout_time   (dict ): the beggining time of payout
        """

        test_count = 0

        responses = []

        payout_time = {"last_payout_time": int(time.time())}

        file_logger = utils.log_to_file('transactions', __name__)


        # Create transactions table if not exists
        create_if_not_exists = """CREATE TABLE IF NOT EXISTS transactions(
                address TEXT,
                payout_amount int,
                success int,
                tx_id int,
                dry_run int,
                message text,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"""

        cursor.execute(create_if_not_exists)
        conn.commit()

        # Iterate through the voter list and transfer calculated amount
        for voter in vote_list:

            payout_amount = int(voter['payout_amount'] * math.pow(10, 8))
            success = False
            tx_id = 0
            dry_run_check = 0
            message = 0

            # Define the payload (amount, secret and recipient)
            payload = {
                'amount': payout_amount,
                'recipientId': voter['address']
            }

            test_count += 1

            if dry_run:

                dry_run_check = 1
                responses.append(payload)

            else:

                payload['secret'] = secret

                # If there is a second passphrase defined include it
                if second_secret:
                    payload['secondSecret'] = second_secret

                # Send the transactions
                address = constants.url + constants.transactions_uri

                response = requests.put(address,
                                        headers=constants.headers,
                                        json=payload).json()

                # Blank out password for return
                del payload['secret']

                if payload.get('secondSecret'):
                    del payload['secondSecret']

                # Update payload dictionary with response information
                payload.update(response)

                # Append payload to return list
                responses.append(payload)

                file_logger.info(payload)

                # add tx_ix and response success from the API to the DB
                success = response['success']

                if success:
                    tx_id = response['transactionId']
                else:
                    message = response['error']

                # Sleep the predetermined time to not flood the network
                time.sleep(delay)

            cursor.execute('''INSERT INTO transactions(address, payout_amount,
                              success, tx_id, dry_run, message) VALUES(?,?,?,?,?,?)''',
                              (voter['wallet_address'], payout_amount,
                               success, tx_id, dry_run_check, message))

        conn.commit()

        return responses, payout_time


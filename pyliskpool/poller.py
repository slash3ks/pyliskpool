#!/usr/bin/env python3

import math
import time
import requests
import logging
from .constants import constants
from .utils import utils

log = logging.getLogger(__name__)

class poller(object):
    """
    Polling related methods
    """

    def get_voters(cursor, conn):
        """
        Get voters given a public key

        Args:
            None
        Returns:
            all_voters (list): list of voters given a public key
            all_voters (None): return none if no response from the API
        """
        all_voters = []

        url = constants.url + constants.getaccount_voters + constants.data.get('delegate')

        limit = 100
        offset = 0
        counter = 0
        voters_all = []
        cont = True

        while cont:
            req_url = "{}&limit={}&offset={}".format(url, limit, offset)
            voters = utils.api_get(req_url)
            if not voters['data']['voters']:
                break
            voters_all += voters['data']['voters']
            counter += 1
            offset += 100
            time.sleep(1)

        # Check if we got a response from the API
        # Iterate through the accounts and append the individual items
        if voters_all:

            # Write all the voters, unedited to a table in the db
            #poller.write_voters(cursor, conn, all_voters)

            mod_voters = []

            for x in voters_all:
                if x['address'] not in constants.ignore['addresses'] and \
                    x['publicKey'] not in constants.ignore['addresses']:

                    mod_voters.append(x)

            all_voters = mod_voters

        return all_voters

    def write_voters(cursor, conn, all_voters):
        """
        Method to write the voters call to a database
        """

        create_if_not_exists = """CREATE TABLE IF NOT EXISTS voters_all (
                address TEXT PRIMARY KEY NOT NULL,
                username TEXT,
                publicKey TEXT)"""

        cursor.execute(create_if_not_exists)

        create_if_not_exists = """create table if not exists voters_bal (
                address text,
                balance int,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"""

        cursor.execute(create_if_not_exists)

        conn.commit()

        for voter in all_voters:

            cursor.execute('''INSERT OR IGNORE INTO voters_all(address, username, publicKey)
                              VALUES(?,?,?)''', (voter['address'], voter['username'], voter['publicKey']))

            cursor.execute('''INSERT INTO voters_bal(address, balance)
                              VALUES(?,?)''', (voter['address'], voter['balance']))
        conn.commit()

    def get_voter_weight(voters):
        """
        Get total vote weight given a pubkey (sum of all voter weight)

        Args:
            None
        Returns:
            total_weight (float): sum of all voter weight
        """
        total_weight = sum([float(x['balance']) / math.pow(10, 8) for x in voters])

        return total_weight

    def get_delegate_rewards(timeframe):
        """
        Get the amount forged by an account based on a public key

        Args:
            timeframe (str ):
        Returns:
            response  (dict):
        """
        url = constants.url
        url += constants.getforgedby_account.format(constants.data.get('delegate_address'),
                                                    constants.data.get('last_payout_time'))

        response = utils.api_get(url)

        return response['data']


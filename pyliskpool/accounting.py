#!/usr/bin/env python3

import math
import json
import datetime
import requests
import logging
from .utils import utils
from .poller import poller
from .constants import constants

log = logging.getLogger(__name__)

class accounting():
    """
    Accounting related methods
    """

    def tx_to_db(data, dry_run, conn, cursor, starttime):
        """
        """
        create_if_not_exists = '''
            CREATE TABLE IF NOT EXISTS "{}"(
            id INTEGER PRIMARY KEY,
            data json,
            dry_run int,
            sent int)
            '''.format(starttime)

        cursor.execute(create_if_not_exists)
        conn.commit()

        sent = 0
        for d in data:
            cursor.execute('''
            INSERT INTO "{}"(data, dry_run, sent) VALUES(?,?,?)
            '''.format(starttime), (json.dumps(d), dry_run, sent))

        conn.commit()

    def forged_to_give(starttime, percentage):
        """
        Determine how much to give based on a timeframe (starting time)

        Args:
            starttime (str ): string with start time in unix epoch time
        Returns:
            N/A       (int ): Amount to give away based on start time
        """

        timeframe = '&start={}'.format(starttime)

        response = poller.get_delegate_rewards(timeframe=timeframe)

        rewards =  float(response['rewards']) / math.pow(10, 8)

        starttime = str(starttime)[:-3]
        from_date = datetime.datetime.fromtimestamp(int(starttime))
        to_date = datetime.datetime.now()
        pay_period = '{} - {}'.format(from_date, to_date)

        log.info("Pay period from: {} to: {}".format(from_date, to_date))
        log.info("Forged for selected timeframe: {}".format(rewards))

        amount = rewards * percentage / 100

        return amount, rewards, pay_period

    def voter_rewards(starttime, percentage, dry_run, cursor, conn, savedir):
        """
        Get the voter rewards (calculation of rewards based on weight)

        Args:
            starttime (str ): string with start time in unix epoch time
        Returns:
            payouts   (list): payout calculation per voting address

        """
        # Start the file logger
        file_logger = utils.log_to_file('details', __name__, savedir)

        create_if_not_exists = """CREATE TABLE IF NOT EXISTS transaction_details(
                pay_period TEXT,
                start_timestamp TEXT,
                rewards int,
                delegate_reward_total int,
                voter_count int,
                payout_tx_fees int,
                reward_minus_fees int,
                dry_run int)"""

        cursor.execute(create_if_not_exists)
        conn.commit()

        # Get the voters of the pubkey account
        voters = poller.get_voters(cursor, conn)

        # Get the total weight of those voters
        total_weight = poller.get_voter_weight(voters)

        # Get the amount to give based on the pool percentage
        # and forge start time
        forged_to_give, rewards, pay_period = accounting.forged_to_give(starttime, percentage)

        # Count the amount of voters to be rewarded and take out
        # the tx fees from the total
        voter_count = len([x for x in voters])
        transaction_fees = voter_count * 0.1
        temp_forged_to_give = forged_to_give - transaction_fees

        file_logger.info("Total Voter count {}".format(voter_count))

        # Here I check that the amount is ok and create a new voter list
        new_voter_list = []

        for voter in voters:

            address = voter['address']
            balance = float(voter['balance'])
            amount = balance / math.pow(10, 8)
            payout = (amount * temp_forged_to_give) / total_weight

            if payout < constants.min_payout:
                continue
            else:
                new_voter_list.append(voter)

        # Recalculate total weight
        new_total_weight = sum([float(x['balance']) / math.pow(10, 8) for x in new_voter_list])

        # Recalculate voter count
        voter_count = len([x for x in new_voter_list])

        # Recalculate transaction fees
        transaction_fees = voter_count * 0.1

        file_logger.info("Elegible Voter count {}".format(voter_count))
        file_logger.info("Total Delegate rewards: {}".format(forged_to_give))

        forged_to_give = forged_to_give - transaction_fees

        file_logger.info("Transaction fees: {}".format(transaction_fees))
        file_logger.info("Delegate rewards: minus tx fee: {}".format(forged_to_give))

        cursor.execute('''
            INSERT INTO transaction_details(pay_period, start_timestamp, rewards,
            delegate_reward_total, voter_count, payout_tx_fees, reward_minus_fees,
            dry_run) VALUES(?,?,?,?,?,?,?,?)''',
            (pay_period, starttime, rewards, forged_to_give + transaction_fees,
            voter_count, transaction_fees, forged_to_give, dry_run))

        conn.commit()

        payouts = []
        for voter in new_voter_list:

            address = voter['address']
            balance = float(voter['balance'])
            amount = balance / math.pow(10, 8)
            payout = (amount * forged_to_give) / new_total_weight

            payout_info = {
                'address': voter['address'],
                'payout_amount': payout
            }

            # append payout
            payouts.append(payout_info)

            file_logger.info(payout_info)

        return payouts

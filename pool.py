#!/usr/bin/env python3

import os
import sys
import math
import time
import argparse
import getpass
import logging
import yaml
import datetime
import dateutil.parser as dtparser
import pyliskpool

LOG = logging.getLogger(__name__)

def _get_data_file(filename):
    """
    """
    try:
        with open(filename) as fh:
            data = yaml.safe_load(fh)
            return data
    except IOError:
        sys.exit("")

def _write_data_file(filename, data):
    """
    """
    try:
        with open(filename, 'w') as fh:
            yaml.safe_dump(data, fh, allow_unicode=True,
                           default_flow_style=False)
            return True
    except IOError:
        sys.exit("")

def get_passphrases(second_phrase):
    """
    """
    secret1_1 = None
    secret2_1 = None

    # prompt for the secret for the payout
    try:
        secret1_1 = getpass.getpass()
        print("Verifying first passphrase. Please enter it again:")
        secret1_2 = getpass.getpass()

        if secret1_1 != secret1_2:
            sys.exit("First pass does not match. please try again.")
    except KeyboardInterrupt:
        sys.exit("Operation aborted. No phrase given")

    if second_phrase:
        try:
            secret2_1 = getpass.getpass()
            print("Verifying second passphrase. Please enter it again:")
            secret2_2 = getpass.getpass()
        except KeyboardInterrupt:
            sys.exit("Operation aborted. No 2nd phrase given")

        if secret2_1 != secret2_2:
            sys.exit("Second pass does not match. please try again.")

    return secret1_1, secret2_1

def create_work_dir():
    """
    Create a folder with the date
    """
    save_dir = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    return save_dir

def get_distrib_table(conn, cursor):
    """
    """
    tablename = None
    statement = "SELECT name FROM sqlite_master WHERE type='table';"
    data = pyliskpool.utils.db_execute(conn, cursor, statement)

    for d in data:
        try:
            tablename = int(d[0])
        except:
            continue

    return tablename

def main(args):
    """
    Submain function for scoping
    """
    # Now timestamp to write as last payout
    ts_now = datetime.datetime.now().isoformat()
    now_timestamp = dtparser.parse(ts_now).strftime('%s%f')[:-3]

    # Get constants/config items from file
    yml_data = _get_data_file(args.filename)

    # DB Connection
    cursor, conn = pyliskpool.utils.open_db()

    # Create working dir if it does not exist
    save_dir = create_work_dir()

    if args.execution_mode == 'make_offline':

        # Grab transactions from the API
        pay_wave = pyliskpool.accounting.voter_rewards(yml_data['last_payout_time'],
                                                       yml_data['percentage'],
                                                       args.dry_run,
                                                       cursor,
                                                       conn,
                                                       save_dir)

        # Write address and amount to the transactions table
        result = pyliskpool.transfer.to_db(pay_wave, cursor, conn, args.dry_run)

        # Modify the last payout time
        yml_data['last_payout_time'] = int(now_timestamp)

        # Write back to the yaml file if not dry run
        _write_data_file(args.filename, yml_data)

    elif args.execution_mode == 'sign_offline':

        # Get the transactions gathered online from the tx table
        result = pyliskpool.transfer.from_db(cursor, conn)

        if not result:
            sys.exit("Did not find any transactions to sign")

        # Get the passphrases from the user
        secret1, secret2 = get_passphrases(args.second_phrase)

        # Generate signed transactions with the secrets provided.
        tx = pyliskpool.offline.generate_transactions(secret1, secret2, result)

        if args.dry_run is False:

            # Save signed transactions to database
            pyliskpool.accounting.tx_to_db(tx,
                                           args.dry_run,
                                           conn,
                                           cursor,
                                           yml_data['last_payout_time'])

            # Alter the transaction table
            statement = 'ALTER TABLE transactions RENAME TO transactions{}'\
                    .format(save_dir.replace('-',''))
            pyliskpool.utils.db_execute(conn, cursor, statement)

    elif args.execution_mode == 'save_offline':

        # Find the table that has the data
        tablename = get_distrib_table(conn, cursor)
        if not tablename:
            sys.exit("Did not find any actionable transactions table")

        # Read the transactions form the timed table
        statement = 'SELECT data from "{}"'.format(tablename)
        tx = pyliskpool.utils.db_execute(conn, cursor, statement)

        # Dump the table with signed transactions to json file
        pyliskpool.offline.dump_to_file(tx, save_dir)

        # Rename the table to done
        statement = 'ALTER TABLE "{0}" RENAME TO "done{0}"'.format(tablename)
        pyliskpool.utils.db_execute(conn, cursor, statement)

    elif args.execution_mode == 'broadcast_fromfile':

        # Get the transactions from the specified file
        pass

        # Send the transaction to the transaction API endpoint

    elif args.execution_mode == 'make_online':

        # Grab all the information from the API
        pay_wave = pyliskpool.accounting.voter_rewards(yml_data['last_payout_time'],
                                                       yml_data['percentage'],
                                                       args.dry_run,
                                                       cursor,
                                                       conn,
                                                       save_dir)

        # Get the passphrases from the user
        secret1, secret2 = get_passphrases(args.second_phrase)

        # Generate signed transactions with the secrets provided.
        tx = pyliskpool.offline.generate_transactions(secret1, secret2, pay_wave)

        # Save transactions to database
        pyliskpool.accounting.tx_to_db(tx,
                                       args.dry_run,
                                       conn,
                                       cursor,
                                       yml_data['last_payout_time'])

        # Modify the last payout time
        yml_data['last_payout_time'] = int(now_timestamp)

        # Write back to the yaml file if not dry run
        _write_data_file(args.filename, yml_data)

    elif args.execution_mode == 'broadcast_online':

        # Find the table that has the data
        tablename = get_distrib_table(conn, cursor)
        if not tablename:
            sys.exit("Did not find any actionable transactions table")

        # Read the transactions form the timed table
        statement = 'SELECT data, id from "{}"'.format(tablename)
        tx = pyliskpool.utils.db_execute(conn, cursor, statement)

        # This is online transaction broadcast
        resp = pyliskpool.transfer.online(tx,
                                          yml_data['interval'],
                                          conn,
                                          cursor,
                                          tablename)

        # Rename the table to done
        statement = 'ALTER TABLE "{0}" RENAME TO "done{0}"'.format(tablename)
        pyliskpool.utils.db_execute(conn, cursor, statement)

    LOG.info(ts_now)

    # Close the database connection
    pyliskpool.utils.close_db(conn)

def usage():
    """
    """

    return '''
    ----------------------------
    #
    python3 pool.py make_offline

    #
    python3 pool.py sign_offline

    #
    python3 pool.py broadcast_online
    ----------------------------

    ----------------------------
    #
    python3 pool.py make_online

    #
    python3 pool.py broadcast_online
    ----------------------------
    '''

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='LISK Pool v1',
                                     usage=usage())

    PARSER.add_argument(dest='execution_mode', action='store',
                        choices=('make_offline', 'sign_offline',
                                 'save_offline','broadcast_offline',
                                 'make_online', 'broadcast_online'),
                        help='')

    PARSER.add_argument('--second-pass', dest='second_phrase',
                        action='store_true', default=False,
                        help='Second passphrase flag')

    PARSER.add_argument('-d', '--dry-run', dest='dry_run',
                        action='store_true', default=False,
                        help='Dry run flag')

    PARSER.add_argument('--offline', dest='offline',
                        action='store_true', help='')

    PARSER.add_argument('--online', dest='online',
                        action='store_true', help='')

    PARSER.add_argument('-t', '--timestamp', dest='timestamp',
                        action='store', help='Timestamp to start')

    PARSER.add_argument('-f', '--filename', dest='filename',
                        default="./config.yaml", action='store',
                        help='Config file. Default is ./config.yaml')

    PARSER.add_argument('-l','--log',dest='log',action='store',
                        choices=('info','debug','warning','error','critical'),
                        help='Logging facility')

    ARGS = PARSER.parse_args()

    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)

    LEVELS = {'debug':logging.DEBUG,
              'info':logging.INFO,
              'warning':logging.WARNING,
              'error':logging.ERROR,
              'critical':logging.CRITICAL,
    }
    if ARGS.log:
        FORMAT = '%(asctime)s %(funcName)s %(message)s '
        DATEFORMAT = '%m/%d/%Y-%I:%M:%S-%p'

        logging.basicConfig(level=LEVELS[ARGS.log],
                            format=FORMAT,
                            datefmt=DATEFORMAT)
    else:
        logging.basicConfig(level=LEVELS["critical"])

    main(ARGS)

